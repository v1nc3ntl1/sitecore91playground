﻿using Sitecore.Pipelines.HttpRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Test.Website.Pipelines
{
    public class EnableSessionStatePipeline : HttpRequestProcessor
    {
        /// <summary>
        /// Pipeline processor implementation.
        /// </summary>
        /// <param name="args">Pipeline processor arguments.</param>
        public override void Process(HttpRequestArgs args)
        {
            //Filter by mytoyota requests, only for page loads and api calls.
            if (HttpContext.Current.Request.AcceptTypes == null
                || !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("vincent")
                || (!HttpContext.Current.Request.AcceptTypes.Contains("text/html") && !HttpContext.Current.Request.AcceptTypes.Contains("application/json"))
                )
            {
                return;
            }

            args.HttpContext.SetSessionStateBehavior(SessionStateBehavior.Required);
        }
    }
}