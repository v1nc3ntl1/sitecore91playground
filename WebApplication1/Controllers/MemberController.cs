﻿using Sitecore.Services.Infrastructure.Web.Http;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AuthenticationManager = Sitecore.Security.Authentication.AuthenticationManager;
namespace Test.Website.Controllers
{
    [RoutePrefix("{sitename:minlength(1)}/vincent/api/v1/member")]
    public class MemberController : ServicesApiController
    {
        // GET api/member/
        [HttpGet]
        [Route("get")]
        public IHttpActionResult Get()
        {
            var currentUser = AuthenticationManager.GetActiveUser();

            if (currentUser == null) throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Not current user found"));

            return Ok(currentUser.DisplayName);
        }

        [HttpPost]
        [Route("Post")]
        // POST api/member
        public IHttpActionResult Post([FromBody]string value)
        {
            // Create virtual user
            var userName = $@"myvincent\{value}_{HttpContext.Current.Session.SessionID}";
            var virtualUser = AuthenticationManager.BuildVirtualUser(userName, true);

            if (virtualUser == null) throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error building virtual user"));

            virtualUser.Profile.SetCustomProperty("SessionToken", "123456789");
            virtualUser.Profile.Email = $"{value}@gmail.com";
            virtualUser.RuntimeSettings.AddedRoles.Add("myvincent\\Member");
            virtualUser.Profile.Save();

            AuthenticationManager.SetActiveUser(virtualUser);
            AuthenticationManager.LoginVirtualUser(virtualUser);

            return Ok(userName);
        }

        // PUT api/member/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/member/5
        public void Delete(int id)
        {

        }
    }
}
