﻿using Sitecore.Services.Infrastructure.Web.Http;
using System.Web.Http;
using Test.Helper;

namespace Test.Website.Controllers
{
    [RoutePrefix("{sitename:minlength(1)}/vincent/api/v1/mock")]
    public class MockController : ServicesApiController
    {
        private SessionWrapper sessionWrapper = new SessionWrapper();

        [HttpGet]
        [Route("get")]
        [Authorize(Roles = "myvincent\\Member")]
        public IHttpActionResult Get()
        {
            return Ok("success");
        }

        [HttpPost]
        [Route("SetSession")]
        [Authorize(Roles = "myvincent\\Member")]
        public IHttpActionResult SetSession(string key, string value)
        {
            sessionWrapper.SetSessionValue(key, value);

            return Ok();
        }

        [HttpGet]
        [Route("GetSession")]
        [Authorize(Roles = "myvincent\\Member")]
        public IHttpActionResult GetSession(string key)
        {
            return Ok(sessionWrapper.GetSessionValue(key));
        }

        [HttpGet]
        [Route("GetData")]
        [Authorize(Roles = "myvincent\\Member")]
        public IHttpActionResult GetSession2(string key)
        {
            return Ok("Test Get Data " + sessionWrapper.GetSessionValue(key));
        }
    }
}
