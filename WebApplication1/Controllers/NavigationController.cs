﻿using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Newtonsoft.Json;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Converters;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Services.Infrastructure.Web.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Http;
using System.Xml.Serialization;
using Test.Helper;

namespace Test.Website.Controllers
{
    [RoutePrefix("{sitename:minlength(1)}/vincent/api/v1/navigation")]
    public class NavigationController : ServicesApiController
    {
        private SessionWrapper sessionWrapper = new SessionWrapper();

        [HttpGet]
        [Route("get")]
        [Authorize(Roles = "myvincent\\Member")]
        public IHttpActionResult Get()
        {
            var contextService = new SitecoreContext();

            var myvincentFolder = contextService.GetItem<Folder>(new Guid("{066DA221-5903-444C-9908-C2A9EFDA1B96}"));

            var menuItems = new List<MenuItem>();

            foreach (var item in myvincentFolder.Children)
            {
                menuItems.Add(new MenuItem() { Name = item.Name, Url = item.Url });
            }

            return Ok(menuItems);
        }

        public class MenuItem
        {
            [JsonProperty("Name", NullValueHandling = NullValueHandling.Ignore)]
            public string Name { get; set; }

            [JsonProperty("Url", NullValueHandling = NullValueHandling.Ignore)]
            public string Url { get; set; }
        }

        public class Folder : SitecoreBaseModel
        {
            [SitecoreChildren(InferType = true)]
            public virtual IEnumerable<SitecoreBaseModel> Children { get; set; }
        }

        [SitecoreType(AutoMap = true)]
        public class SitecoreBaseModel : ISitecoreBaseModel
        {
            [SitecoreInfo(SitecoreInfoType.Name)]
            public virtual string Name { get; set; }

            [TypeConverter(typeof(IndexFieldItemUriValueConverter))]
            [XmlIgnore]
            [IndexField("_uniqueid")]
            public virtual ItemUri Uri { get; set; }

            [SitecoreInfo(SitecoreInfoType.Version)]
            public virtual int Version
            {
                get { return Uri == null ? 0 : Uri.Version.Number; }
            }

            [SitecoreField("__Sortorder")]
            public virtual string Sortorder { get; set; }

            [SitecoreField("__Created")]
            public virtual DateTime Created { get; set; }

            [SitecoreInfo(SitecoreInfoType.BaseTemplateIds)]
            [IndexField("_templates")]
            [TypeConverter(typeof(IndexFieldEnumerableConverter))]
            public IEnumerable<ID> BaseTemplates { get; set; }


            [IndexField("_path")]
            [TypeConverter(typeof(IndexFieldEnumerableConverter))]
            public IEnumerable<ID> Paths { get; set; }

            [IndexField("_latestversion")]
            public bool IsLatestVersion { get; set; }

            [SitecoreId]
            [IndexField("_group")]
            [TypeConverter(typeof(IndexFieldGuidValueConverter))]
            public virtual Guid Id { get; set; }

            [SitecoreInfo(SitecoreInfoType.TemplateId)]
            public virtual Guid TemplateId { get; set; }

            [SitecoreInfo(SitecoreInfoType.Language)]
            [IndexField("_language")]
            public virtual string Language { get; set; }

            [SitecoreInfo(SitecoreInfoType.Path)]
            public virtual string Path { get; set; }

            [SitecoreInfo(SitecoreInfoType.Url)]
            public virtual string Url { get; set; }

            [SitecoreParent]
            public virtual Item Parent { get; set; }

        }

        [SitecoreType(AutoMap = true)]
        public interface ISitecoreBaseModel
        {
            [SitecoreId]
            Guid Id { get; set; }

            [SitecoreInfo(SitecoreInfoType.Url)]
            string Url { get; set; }

            [SitecoreInfo(SitecoreInfoType.Path)]
            string Path { get; set; }

            [SitecoreInfo(SitecoreInfoType.TemplateId)]
            Guid TemplateId { get; set; }

            string Language { get; set; }

            Item Parent { get; set; }

        }
    }
}
