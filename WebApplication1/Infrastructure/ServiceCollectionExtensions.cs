﻿using Microsoft.Extensions.DependencyInjection;
using Sitecore.Services.Infrastructure.Web.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Test.Website.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        [MethodImplAttribute(MethodImplOptions.NoInlining)]
        public static void AddMvcControllersInCurrentAssembly(this IServiceCollection serviceCollection)
        {
            AddMvcControllers(serviceCollection, Assembly.GetCallingAssembly());
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void AddMvcControllersInAssembly(this IServiceCollection serviceCollection, string assembly)
        {
            AddMvcControllers(serviceCollection, GetAssemblyByName(assembly));
        }

        public static IServiceCollection AddOfType<T>(this IServiceCollection serviceCollection, string assemblyPattern)
        {
            var assemblies = GetAssemblies(assemblyPattern);
            Type[] types = GetTypesImplementing<T>(assemblies.ToArray());
            types.ToList().ForEach(x =>
            {
                serviceCollection.AddTransient(typeof(T), x);
            });
            return serviceCollection;
        }

        private static Assembly GetAssemblyByName(string name)
        {
            return AppDomain.CurrentDomain.GetAssemblies().
                   SingleOrDefault(assembly => assembly.GetName().Name == name);
        }

        private static List<Assembly> GetAssemblies(string pattern)
        {
            if (pattern == "*")
            {
                throw new Exception("You cannot reflect the entire Bin DIR! This will make Sitecore startup too slow");
            }

            List<Assembly> allAssemblies = new List<Assembly>();
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            foreach (string dll in Directory.GetFiles(path, pattern + ".dll"))
            {
                allAssemblies.Add(Assembly.LoadFile(dll));
            }

            return allAssemblies;
        }

        private static void AddMvcControllers(this IServiceCollection serviceCollection, params Assembly[] assemblies)
        {
            foreach (Assembly assembly in assemblies)
            {
                serviceCollection.AddOfType<IController>(assembly);
                serviceCollection.AddOfType<ApiController>(assembly);
                serviceCollection.AddOfType<ServicesApiController>(assembly);
            }
        }

        public static void AddOfType<T>(this IServiceCollection serviceCollection, Assembly assembly)
        {
            var controllers = GetTypesImplementing<T>(assembly)
                .Where(controller => controller.Name.EndsWith("Controller", StringComparison.Ordinal));

            foreach (var controller in controllers)
            {
                serviceCollection.AddTransient(controller);
            }
        }

        private static Type[] GetTypesImplementing<T>(params Assembly[] assemblies)
        {
            if (assemblies == null || assemblies.Length == 0)
            {
                return new Type[0];
            }

            var targetType = typeof(T);

            return assemblies
                .Where(assembly => !assembly.IsDynamic)
                .SelectMany(GetExportedTypes)
                .Where(type => !type.IsAbstract && !type.IsGenericTypeDefinition && targetType.IsAssignableFrom(type))
                .ToArray();
        }

        private static IEnumerable<Type> GetExportedTypes(Assembly assembly)
        {
            try
            {
                return assembly.GetExportedTypes();
            }
            catch (NotSupportedException)
            {
                // A type load exception would typically happen on an Anonymously Hosted DynamicMethods
                // Assembly and it would be safe to skip this exception.
                return Type.EmptyTypes;
            }
            catch (ReflectionTypeLoadException ex)
            {
                // Return the types that could be loaded. Types can contain null values.
                return ex.Types.Where(type => type != null);
            }
            catch (Exception ex)
            {
                // Throw a more descriptive message containing the name of the assembly.
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture,
                    "Unable to load types from assembly {0}. {1}", assembly.FullName, ex.Message), ex);
            }
        }
    }
}