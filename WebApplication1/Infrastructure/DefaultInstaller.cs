﻿using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Test.Website.Infrastructure
{
    public class DefaultInstallers : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddMvcControllersInAssembly("Test.Website");
        }
    }
}