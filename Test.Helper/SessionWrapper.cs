﻿using System.Web;

namespace Test.Helper
{
    public class SessionWrapper
    {
        private readonly string prefix = "test-vincent-";

        public void SetSessionValue(string Key, string value)
        {
            HttpContext.Current.Session[string.Concat(prefix, Key)] = value;
        }

        public string GetSessionValue(string Key)
        {
            if (HttpContext.Current?.Session[string.Concat(prefix, Key)] == null) return null;

            return HttpContext.Current.Session[string.Concat(prefix, Key)].ToString();
        }
    }
}
